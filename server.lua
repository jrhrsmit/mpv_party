
function start()
    socket = require("socket")
    server = assert(socket.bind("*", 7331))
    ip, port = server:getsockname()
    
    print("Server started on " .. ip .. ":" .. tostring(port))
    totalclient = 0 -- store the total connections
    clients = {} -- e.g. clients[1] = 0xSocketAddress
    clientnames = {} -- e.g. clientnames[1] = "john"
    -- new client connected, settimeout to not block connetcion
    server:settimeout(0.01)
    -- start the loop to listening connection
    mp.add_periodic_timer(0.5, routine)
end

function routine()
    local client, err = server:accept();

    -- print the info of new client and add new client to the table
    if (not err) then
        client:settimeout(4) -- prevent blocking
        clientname,err = client:receive()
        if (not err) then
            totalclient = totalclient + 1
            clients[totalclient] = client
            clientnames[totalclient] = clientname
            print(">> "..clientname.." connected from " .. tostring(client:getsockname()) .." at " ..os.date("%m/%d/%Y %H:%M:%S"))
        else
            client:close()
        end
    end

    paused = mp.get_property_native("pause")
    time_pos = mp.get_property("time-pos")
    -- loop through the client table
    for i = 1, totalclient do
        -- if there is client, listen to that client
        if (clients[i] ~= nil) then
            clients[totalclient]:settimeout(0.01) -- prevent blocking
            clientmessage, err = clients[i]:receive() -- accept data from client
            -- check if there is something sent to server, and no error occured
            if (err == nil and clientmessage ~= nil) then
                -- loop through the list of client to send broadcast message
                print(clientnames[i] .. ": \"" .. clientmessage .. "\"")
            end
        end
        -- send status each second
        if paused then
            clients[i]:send("paus " .. tostring(time_pos) .. "\n")
        else
            clients[i]:send("play " .. tostring(time_pos) .. "\n")
        end
    end
end

mp.add_key_binding("F12", "mpv_party_server_start", start)
