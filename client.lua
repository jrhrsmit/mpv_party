function start()
    host, port = "pboy.nl", 7331
    socket = require("socket")
    tcp = assert(socket.tcp())
    
    tcp:connect(host, port)
    tcp:send("lmao\n")
    tcp:settimeout(1)
    print("Client started for " .. host .. ":" .. tostring(port))

    -- start the loop to listening connection
    mp.add_periodic_timer(0.5, routine)
end

function routine()
    local s, status, partial = tcp:receive()
    if status == "closed" then
        tcp:close()
        tcp:connect(host, port);
        tcp:send("lmao\n");
    else
        local paused = mp.get_property_bool("pause")
        if paused == true and s:sub(0, 4) == "play" then
            print("Server playing")
            mp.set_property_bool("pause", false)
        else 
            if paused == false and s:sub(0, 4) == "paus" then
                print("Server paused")
                mp.set_property_bool("pause", true)
            end
        end
        local time_pos_ref = tonumber(s:sub(5))
        local time_pos = mp.get_property("time-pos")
        if math.abs(time_pos_ref - time_pos) > 1 then
            print("Corrected time from reference form server")
            mp.set_property("time-pos", time_pos_ref)
        end
    end
end

mp.add_key_binding("F11", "mpv_party_server_start", start)
