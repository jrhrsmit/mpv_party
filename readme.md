# MPV Party
## Installation Linux
Install the following packages:
- `mpv`
- `lua`
- `lua52-socket`

and run `./install_linux` or just copy `client.lua` to `~/.config/mpv/scripts/` yourself.

## Installation Windows
- install Lua https://github.com/rjpcomputing/luaforwindows/releases/download/v5.1.5-52/LuaForWindows_v5.1.5-52.exe
- install mpv 32 bit! https://sourceforge.net/projects/mpv-player-windows/files/32bit/mpv-i686-20201025-git-49d6a1e.7z/download

## Usage
- Press F11 to start the client connection
- Press F12 to start the server
The client connects to pboy.nl:7331, on which mpv with the server is running

## protocol
- only server -> client
- sends text "play hh:mm:ss" if playing
- sends text "paus hh:mm:ss" if paused
